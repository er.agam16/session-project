from flask import Flask
from flaskext.mysql import MySQL
import json
from flask import Response, request
from passlib.hash import sha256_crypt
import jwt
from flask_cors import CORS
import time

app = Flask(__name__)
CORS(app)

def checkAuth(jwttoken):
	print(jwttoken)
	if (jwttoken):
		try:
			decoded= jwt.decode(jwttoken, 'memMEBER&&##Data$@#^&#@&#^@&#',  algorithms=['HS256'])
			return True
		except:
			return False
		return True
	else:
		return False


def encryptPassword(password):
	return sha256_crypt.hash(password)


def verifyPasswords(password, dbpassword):
	return sha256_crypt.verify(password, dbpassword)


# connection to mysql
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Nexus161990@'
app.config['MYSQL_DATABASE_DB'] = 'full_stack_session'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/members', methods = ['GET'])
def membersList():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT firstName, lastName, reason_for_payment, amount, payment_months, date, received_by from membersForm;")
    returnData = []
    data = cursor.fetchall()
    for info in data:
        userObject = {"firstName": info[0],"lastName": info[1], "reason_for_payment": info[2], "amount": info[3], "payment_months": info[4],"date":info[5],"received_by": info[6]}
        returnData.append(userObject)
        jsonResult = json.dumps(returnData, indent=4, sort_keys=True, default=str)
	return Response(jsonResult, mimetype ='application/json')
    else:
	    return Response(json.dumps({"status": False, "message": "Token not valid"}), mimetype='application/json')


@app.route('/members', methods = ['POST'])
def addMembers():
    conn = mysql.connect()
    cursor = conn.cursor()

    data_members = json.loads(request.data)
    firstName = data_members.get('first_name', None)
    lastName = data_members.get('last_name', None)
    reason_for_payment = data_members.get('reason_for_payment', None)
    amount = data_members.get('amount', None)
    payment_months = data_members.get('payment_months', None)
    date = data_members.get('date', None)
    received_by = data_members.get('received_by', None)


    if not firstName:
        return Response(json.dumps({'status': False, 'message':'First Name is required'}), mimetype='application/json')
    if not lastName:
        return Response(json.dumps({'status': False, 'message':'Last Name is required'}), mimetype='application/json')
    if not reason_for_payment:
        return Response(json.dumps({'status': False,'message':'Reason for payment is required'}), mimetype='application/json')
    if not amount:
        return Response(json.dumps({'status': False, 'message':'Amount of payment is required'}), mimetype='application/json')
    try:
        cursor.execute("INSERT INTO membersForm (firstName, lastName, reason_for_payment, amount, payment_months, date, received_by) VALUE (%s,%s,%s,%s,%s,%s,%s)",(firstName, lastName, reason_for_payment, amount, payment_months, date, received_by))
        conn.commit()
        return Response(json.dumps({"status":True}), mimetype='application/json')
    except Exception, e:
       return Response(json.dumps({"status":False, 'message': str(e)}), mimetype='application/json')


@app.route('/login', methods = ['POST'])
def login():
    conn = mysql.connect()
    cursor = conn.cursor()

    data_members = json.loads(request.data)
    username = data_members.get('username', None)
    password = data_members.get('password', None)

    if not username:
		return Response(json.dumps({"status": False, "message": "User Name Required"}), mimetype='application/json')

    if not password:
		return Response(json.dumps({"status": False, "message": "Password Required"}), mimetype='application/json')
    try:
        cursor.execute("SELECT * from users WHERE username = %s", (username))
        data = cursor.fetchone()
        if data is None:
            return Response(json.dumps({"status": False, "message": "Username not valid"}), mimetype='application/json')
        # Till here we know the username exists. But now we need to check for password as well
        dbpassword = data[5]
        isValidPassword = verifyPasswords(password, dbpassword)
        if not isValidPassword:
            return Response(json.dumps({"status": False, "message": "Invalid Password"}), mimetype='application/json')

        returnUserData = {
            "id": data[3],
            "name": data[0],
            "birthdate": time.mktime(data[2].timetuple()) if data[2] else False,
            "sex": data[1]
        }
        # Here wew are creating the token
        encoded = jwt.encode(returnUserData, 'TESTINGtest1234$@#^&#@&#^@&#', algorithm='HS256')

        return Response(json.dumps({"status": True, "token": encoded}), mimetype='application/json')
    except Exception as e:
        return Response(json.dumps({"status": False, "message": str(e)}), mimetype='application/json')



@app.route('/social-login', methods = ['POST'])
def SocialLogin():
    conn = mysql.connect()
    cursor = conn.cursor()

    data_members = json.loads(request.data)
    id_user = data_members.get('id', None)
    email = data_members.get('email', None)
    image = data_members.get('image', None)
    name = data_members.get('name', None)


    if not id_user:
        return Response(json.dumps({"status": False, "message": "Facebook ID Required"}), mimetype='application/json')

    if not email:
        return Response(json.dumps({"status": False, "message": "Email Required"}), mimetype='application/json')
    try:
        cursor.execute("SELECT * from users WHERE fb_id = %s", (id_user))
        data = cursor.fetchone()
        if data is None:
            try:
                cursor.execute("INSERT INTO users (name, sex, username, password, fb_id, image, email) VALUES (%s, %s, %s, %s, %s, %s, %s)",(name, 'D', '', '', id_user, image, email))
                conn.commit()
                cursor.execute("SELECT * from users WHERE fb_id = %s", (id_user))
                data = cursor.fetchone()
            except Exception, e:
                return Response(json.dumps({"status": False, "message": str(e)}), mimetype='application/json')
        
        returnUserData = {
            "id": data[3],
            "name": data[0],
            "birthdate": False,
            "sex": data[1],
            "fb_id": data[5],
            "image": data[6],
            "email": data[7],
        }
        # Here wew are creating the token
        encoded = jwt.encode(returnUserData, 'TESTINGtest1234$@#^&#@&#^@&#', algorithm='HS256')

        return Response(json.dumps({"status": True, "token": encoded}), mimetype='application/json')
    except Exception as e:
        return Response(json.dumps({"status": False, "message": str(e)}), mimetype='application/json')

