import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication.service';
// import{LocalStorageService} from 'angular-2-local-storage';
import {
  AuthService,
  FacebookLoginProvider
} from 'angular-6-social-login';
import { JwtHelperService } from '@auth0/angular-jwt';
import {LocalStorageService} from 'angular-2-local-storage';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: object;
  errorMsg: string;

  constructor(
    private router: Router, private auth: AuthenticationService, private socialAuthService: AuthService, private lStore: LocalStorageService) {
  // creating empty variable
    this.user = {
      'username': '',
      'password': ''
    };
    console.log('hello');
   }

  ngOnInit() {

  }

  facebookSignIn() {
    const socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    console.log('1');
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (fbData) => {
        console.log('2');
        console.log('Facebook sign in data : ' , fbData);
        this.auth.socialLogin(fbData).subscribe((data) => {
          if (data['status']) {
            this.errorMsg = '';
            const decodedToken = jwt_decode(data['token']);
            this.lStore.add('USER_LOGGED_IN_KEY', data['token']);
            // this.lStore.add('LOGGED_USER_DATA', decodedToken);
            this.auth.authenticationState.next(decodedToken);
            this.router.navigate(['/members']);
          } else {
            this.errorMsg = data['message'];
          }
        });


      }
    );
    console.log('3');
  }

  handleSubmit() {
    console.log('clicked');
    this.auth.login(this.user['username'], this.user['password']).subscribe((data) => {
      console.log('i am in first callback');
      if (data['status']) {
        this.errorMsg = '';
        const decodedToken = jwt_decode(data['token']);
        this.auth.authenticationState.next(decodedToken);
        this.lStore.add('USER_LOGGED_IN_KEY', data['token']);
        // this.lStore.add('LOGGED_USER_DATA', decodedToken);
        this.router.navigate(['/members']);
      } else {
        this.errorMsg = data['message'];
      }
    }, (err) => {
        console.log('i am in error callback');
        this.errorMsg = err.message;
        console.log(err);
    });
  }
}



