import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {MembersComponent} from './members/members.component';
import {MembersListComponent} from './members-list/members-list.component';
import {AuthGuard} from './auth.guard';


const routes: Routes = [
  {path: '', component: MembersComponent, canActivate: [AuthGuard], pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'members', component: MembersComponent, canActivate: [AuthGuard]},
  {path: 'membersList', component: MembersListComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
