import { Component } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {LocalStorageService} from 'angular-2-local-storage';
import {Router} from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'selfPractice';

  userObj: any;

  constructor(private authService: AuthenticationService, private lStore: LocalStorageService, private router: Router) {
    this.userObj = false;
    if (this.lStore.get('USER_LOGGED_IN_KEY')) {
      this.authService.authenticationState.next(jwt_decode(this.lStore.get('USER_LOGGED_IN_KEY')));
    }

    this.authService.authenticationState.subscribe((data) => {
      if (data === false) {
        this.userObj = '';
        this.router.navigate(['/login']);
      } else {
        this.userObj = data;
      }
      console.log(this.userObj);
      console.log(data);
    });


  }


  logout() {
    this.lStore.remove('USER_LOGGED_IN_KEY');
    this.authService.authenticationState.next(false);
  }





}
