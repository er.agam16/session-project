import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  memberData: any;
  reasonArray: any;

  constructor(private router: Router) {
    this.reasonArray = ['Donation', 'ABC', 'DEF'];
    this.memberData = {
      'first_name': '',
      'last_name': '',
      'reason': '',
      'payment_month': '',
      'amount': 0
    };
  }

  ngOnInit() {

  }


  submitMemberForm() {
    console.log(this.memberData);
    // this.router.navigate(['/membersList']);
  }

}
